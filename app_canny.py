import gradio as gr
from model import Model

def create_demo(model: Model):

    with gr.Blocks() as demo:
        with gr.Row():
            gr.Markdown('## Text and Canny-Edge Conditional Video Generation (coming soon)')
    return demo
