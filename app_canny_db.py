import gradio as gr
from model import Model
import gradio_utils


def create_demo(model: Model):

    with gr.Blocks() as demo:
        with gr.Row():
            gr.Markdown('## Text, Canny-Edge and DreamBooth Conditional Video Generation (coming soon)')
    return demo
