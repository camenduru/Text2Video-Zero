---
title: Text2Video-Zero
emoji: 🚀
colorFrom: green
colorTo: blue
sdk: gradio
sdk_version: 3.23.0
app_file: app.py
pinned: false
---