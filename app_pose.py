import gradio as gr
import os

from model import Model

def create_demo(model: Model):
    with gr.Blocks() as demo:
        with gr.Row():
            gr.Markdown('## Text and Pose Conditional Video Generation (coming soon)')

    return demo
